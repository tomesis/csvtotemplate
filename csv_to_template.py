# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 16:15:25 2016

@author: e_c_gow
"""

from tkinter import Tk, RIGHT, LEFT, BOTH
from tkinter import X, END, Checkbutton, BooleanVar
from tkinter.ttk import Frame, Button, Style, Label, Entry
from tkinter.filedialog import askopenfilename, askdirectory
import csv
import re
import os
from tkinter import messagebox


class csv_to_template(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)

        self.parent = parent
        self.initUI()

    def initUI(self):

        self.parent.title("CSV to Template tool")
        self.style = Style()
        self.style.theme_use("default")
        self.pack(fill=BOTH, expand=1)

        frame1 = Frame(self)
        frame1.pack(fill=BOTH, expand=True)

        lbl1 = Label(frame1, text="XML template", width=14)
        lbl1.pack(side=LEFT, padx=5, pady=5)

        selectXMLButton = Button(frame1, text="Browse",
                                 command=self.selectXML)
        selectXMLButton.pack(side=RIGHT, padx=5)

        self.entry1 = Entry(frame1)
        self.entry1.pack(fill=X, padx=5, expand=True)

        frame2 = Frame(self)
        frame2.pack(fill=BOTH, expand=True)

        lbl2 = Label(frame2, text="CSV template", width=14)
        lbl2.pack(side=LEFT, padx=5, pady=5)

        selectCSVButton = Button(frame2, text="Browse",
                                 command=self.selectCSV)
        selectCSVButton.pack(side=RIGHT, padx=5)

        self.entry2 = Entry(frame2)
        self.entry2.pack(fill=X, padx=5, expand=True)

        frame3 = Frame(self)
        frame3.pack(fill=BOTH, expand=True)

        lbl3 = Label(frame3, text="Output", width=14)
        lbl3.pack(side=LEFT, padx=5, pady=5)

        selectOutButton = Button(frame3, text="Browse",
                                 command=self.selectOut)
        selectOutButton.pack(side=RIGHT, padx=5)

        self.entry3 = Entry(frame3)
        self.entry3.pack(fill=X, padx=5, expand=True)

        frame5 = Frame(self)
        frame5.pack(fill=BOTH, expand=True)

        self.singleFile = BooleanVar()
        self.cb_single = Checkbutton(frame5, text="Single Output File",
                                     variable=self.singleFile,
                                     command=self.checkSingle)
        self.cb_single.pack(side=LEFT, padx=5, pady=5)

        frame4 = Frame(self)
        frame4.pack(fill=BOTH, expand=True)

        quitButton = Button(frame4, text="Quit", command=self.quit)
        quitButton.pack(side=RIGHT, padx=5, pady=5)

        replaceButton = Button(frame4, text="Replace",
                               command=self.doReplacements)
        replaceButton.pack(side=RIGHT)

    def selectXML(self):
        infilename = askopenfilename(title='Pick the XML template file...')
        self.entry1.delete(0, END)
        self.entry1.insert(0, infilename)

        # get the folder of the xml file to use for output location
        outfolder = os.path.split(infilename)[0]
        self.entry3.delete(0, END)

        # update output to same folder
        self.entry3.insert(0, outfolder)

    def selectCSV(self):
        infilename = askopenfilename(title='Pick the CSV template file...')
        self.entry2.delete(0, END)
        self.entry2.insert(0, infilename)

    def selectOut(self):
        outfolder = askdirectory(title='Pick the output folder...')
        # if multiple files then truncate this to just the folder
        self.entry3.delete(0, END)
        self.entry3.insert(0, outfolder)

    def checkSingle(self):
        pass

    def doReplacements(self):
        # get file locations
        infilename = self.entry1.get()
        dictfilename = self.entry2.get()
        outfileroot = self.entry3.get()
        if self.singleFile.get():
            answer = 'no'
        else:
            answer = 'yes'

        # read csv file and create dict
        with open(dictfilename, 'r', newline='', encoding="utf8") as dictfile:
            data = filter(None, csv.reader(dictfile))
            data = list(data)

        headers = data[0]  # Get the headers from the first row
        filename_index = None
        try:
            filename_index = headers.index("filename")
        except ValueError:
            pass  # The "filename" column is not found

        # find how many rows of data:
        ndata = len(data) - 1  # first row is header

        # loop over rows of data
        for i in range(ndata):
            # create dictionary from first row (header info) and row i
            mydict = dict(zip(*[data[:][0], data[:][i + 1]]))

            for key, value in mydict.items():
                if not value or value.strip() == '':
                    mydict[key] = 'NO VALUE'
                else:
                    mydict[key] = value.strip()

            # create output filename (multiple files)
            if answer == 'yes':
                if filename_index is not None:
                    outfilename = os.path.join(outfileroot, mydict['filename'])
                else:
                    outfilename = os.path.join(outfileroot, 
                                            mydict['<!-- insert idno -->'].
                                            replace(" ", "_"))

                # Add ".xml" extension if not already present
                if not outfilename.endswith(".xml"):
                    outfilename += ".xml"

                print(str(i) + " of " + str(ndata) + " : " + outfilename)
                filemode = 'w'

            # create output filename (single file)
            else:
                outfilename = os.path.join(outfileroot, os.path.split(dictfilename)[1].replace('.csv','_output.xml')) 
                if i == 0:
                    filemode = 'w'
                else:
                    filemode = 'a'

            # check if output file already exists (only first time round)
            if i == 0:
                if os.path.isfile(outfilename):
                    # ask if overwrite is OK
                    overwrite = messagebox.askquestion("Warning",
                                                         "Output file " +
                                                         "already exists. " +
                                                         " Do you want to " +
                                                         "overwrite?",
                                                         icon='warning')
                    if overwrite == 'no':
                        return

            # create pattern object from dict for matching
            pattern = re.compile('|'.join(mydict.keys()))

            # open input and output files
            with open(infilename) as infile:
                with open(outfilename, filemode, encoding="utf8") as outfile:
                    # loop over lines in infile
                    for line in infile:
                        # replace substrings in s according to defined
                        # pattern object
                        result = pattern.sub(lambda x: mydict[x.group()], line)
                        # write result to outfile
                        outfile.write(result)

        messagebox.showinfo("Finished", "Done :)")
        self.quit()


def main():

    root = Tk()
    root.geometry("650x250+300+300")
    csv_to_template(root)
    root.mainloop()


if __name__ == '__main__':
    main()
