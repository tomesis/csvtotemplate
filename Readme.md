## csv_to_template.py ##

Simple application to replace strings (codes) in a template file (e.g. XML)
according to a dictionary created from an input CSV file.
The CSV file can have many rows. The top row defines the codes to be replaced.
Subsequent rows define a set of replacements.
The program iterates through the file one row at a time. For each row a copy of
the template is created with the appropriate replacements.
The user can choose whether all copies are concatenated in one file or saved as
separate files.

Simplest way to run is with python installed. From a command window type

>> python csv_to_template.py

Also provided is a windows executable created with pyinstaller. This can be
launched by double clicking, but is slower as it takes a while to launch.
